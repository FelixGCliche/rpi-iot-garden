package csf.v50.iotgardenapi.services

import csf.v50.iotgardenapi.model.SoilMoisture
import csf.v50.iotgardenapi.repository.SoilMoistureRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service

@Service("SoilMoistureService")
class SoilMoistureService: ISensorService<SoilMoisture> {

    @Autowired
    private lateinit var repository: SoilMoistureRepository

    override fun GetAllPaginated(pageable: Pageable): Page<SoilMoisture> {
        return repository.findAll(pageable)
    }

    override fun getById(id: String): SoilMoisture {
        return repository.findById(id).get()
    }

    override fun create(obj: SoilMoisture): SoilMoisture {
        return repository.save(obj)
    }
}