package csf.v50.iotgardenapi.services

import csf.v50.iotgardenapi.model.AirHumidity
import csf.v50.iotgardenapi.repository.AirHumidityRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository
import org.springframework.stereotype.Service


@Service("AirHumidityService")
class AirHumidityService: ISensorService<AirHumidity> {

    @Autowired
    private lateinit var repository: AirHumidityRepository

    override fun GetAllPaginated(pageable: Pageable): Page<AirHumidity> {
        return repository.findAll(pageable)
    }

    override fun getById(id: String): AirHumidity {
        return repository.findById(id).get()
    }

    override fun create(obj: AirHumidity): AirHumidity {
        return repository.save(obj)
    }
}