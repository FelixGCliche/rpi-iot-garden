package csf.v50.iotgardenapi.repository

import csf.v50.iotgardenapi.model.SoilMoisture
import org.springframework.data.mongodb.repository.MongoRepository

interface SoilMoistureRepository: MongoRepository<SoilMoisture, String>