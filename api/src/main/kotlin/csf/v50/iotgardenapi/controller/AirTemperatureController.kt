package csf.v50.iotgardenapi.controller

import csf.v50.iotgardenapi.model.AirTemperature
import csf.v50.iotgardenapi.services.AirTemperatureService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("air-temperature")
class AirTemperatureController: ISensorController<AirTemperature> {

    @Autowired
    private lateinit var service: AirTemperatureService

    override fun getAllPaginated(page: Int, size: Int): ResponseEntity<Page<AirTemperature>> {
        val task: Page<AirTemperature>

        try {
            task = service.GetAllPaginated(PageRequest.of(page, size))
            return ResponseEntity(task, HttpStatus.CREATED)
        } catch (e: Exception) {
            return ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }

    override fun getById(id: String): ResponseEntity<AirTemperature> {
        val task: AirTemperature

        try {
            task = service.getById(id)
            return ResponseEntity(task, HttpStatus.CREATED)
        } catch (e: Exception) {
            return ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }

    override fun save(obj: AirTemperature): ResponseEntity<AirTemperature> {
        val task: AirTemperature

        try {
            task = service.create(obj)
            return ResponseEntity(task, HttpStatus.CREATED)
        } catch (e: Exception) {
            return ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }
}