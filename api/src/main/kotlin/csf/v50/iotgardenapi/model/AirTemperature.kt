package csf.v50.iotgardenapi.model

import org.bson.types.ObjectId

data class AirTemperature(
        override val _id: ObjectId = ObjectId(),
        override var value: Double = 0.0
) : ISensorData