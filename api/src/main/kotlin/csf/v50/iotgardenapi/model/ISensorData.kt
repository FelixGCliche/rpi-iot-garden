package csf.v50.iotgardenapi.model

import org.bson.types.ObjectId

interface ISensorData {
    val _id: ObjectId
    var value: Double
}