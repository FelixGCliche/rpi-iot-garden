import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:iot_garden_flutter/model/AirHumidity.dart';
import 'package:iot_garden_flutter/service/NetworkCall.dart';
import 'package:iot_garden_flutter/viewmodel/AirHumidityVizualizerViewModel.dart';
import 'package:iot_garden_flutter/viewmodel/SensorDataVisualizerViewModel.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class AirHumidityChartView extends StatefulWidget {
  AirHumidityChartView({Key key}) : super(key: key);

  @override
  State<AirHumidityChartView> createState() => AirHumidityChartViewState();
}

class AirHumidityChartViewState extends State<AirHumidityChartView> {

  SensorDataVisualizerViewModel<AirHumidity> viewModel;

  @override
  void initState() {
    super.initState();
    this.viewModel = AirHumidityVizualizerViewModel(
    "Air humidity", "Time", "Humidity %", 0, 100, 10);
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(child: getDataChart()),
    );
  }

  SfCartesianChart getDataChart() {
    return SfCartesianChart(
        plotAreaBorderWidth: 0,
        title: ChartTitle(text: viewModel.chartTitle),
        primaryXAxis: DateTimeAxis(majorGridLines: MajorGridLines(width: 0)),
        primaryYAxis: NumericAxis(
          minimum: viewModel.minValue,
          maximum: viewModel.maxValue,
          interval: viewModel.interval,
          labelFormat: '{value}\%',
          title: AxisTitle(text: viewModel.yAxisTitle),
          axisLine: AxisLine(width: 0),
          majorTickLines: MajorTickLines(size: 0),
        ),
        series: viewModel.getStaticLineSeries(),
        trackballBehavior: TrackballBehavior(
            enable: true,
            activationMode: ActivationMode.singleTap,
            tooltipSettings: InteractiveTooltip(
                format: 'point.x : point.y', borderWidth: 0)));
  }
}
