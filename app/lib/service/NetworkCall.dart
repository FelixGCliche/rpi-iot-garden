import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:http/http.dart';

import 'ApiException.dart';

class NetworkCall {
  final String apiRoot = "http://34.95.55.215/";

  Future<String> getDefaultString() async {
    final response = await get(apiRoot);
    return response.body.toString();
  }

  Future<dynamic> getAllPaginated(String type, int page, int size) async {
    var jsonResponse;
    try {
      final response = await get(apiRoot + type + "?page=$page&size=$size");
      jsonResponse = _returnResponse(response);
    } on SocketException {
      throw FetchDataException("No internet connection");
    }
    return jsonResponse;
  }

  dynamic _returnResponse(Response response) {
    if (response.statusCode >= 200 && response.statusCode < 300) {
      var jsonResponse = json.decode(response.body.toString());
      log(jsonResponse);
      return jsonResponse;
    }
    if (response.statusCode == 400)
      throw BadRequestException(response.body.toString());
    if (response.statusCode == 401)
      throw UnauthorisedException(response.body.toString());
    if (response.statusCode == 403)
      throw UnauthorisedException(response.body.toString());
    if (response.statusCode == 500)
      throw BadRequestException(response.body.toString());
    else
      throw FetchDataException(
          'Error occured while Communication with Server with StatusCode : ${response.statusCode}');
  }
}
