import 'package:json_annotation/json_annotation.dart';
import 'package:iot_garden_flutter/model/ISensorData.dart';

part 'AirHumidity.g.dart';

@JsonSerializable()
class AirHumidity implements ISensorData {
  @override
  @JsonKey(name: '_id', required: true)
  String id;
  @override
  double value;

  AirHumidity(this.id, this.value);

  factory AirHumidity.fromJson(Map<String, dynamic> json) =>
      _$AirHumidityFromJson(json);

  Map<String, dynamic> toJson() => _$AirHumidityToJson(this);
}
