// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'AirHumidity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AirHumidity _$AirHumidityFromJson(Map<String, dynamic> json) {
  $checkKeys(json, requiredKeys: const ['_id']);
  return AirHumidity(
    json['_id'] as String,
    (json['value'] as num)?.toDouble(),
  );
}

Map<String, dynamic> _$AirHumidityToJson(AirHumidity instance) =>
    <String, dynamic>{
      '_id': instance.id,
      'value': instance.value,
    };
